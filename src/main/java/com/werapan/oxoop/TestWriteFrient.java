package com.werapan.oxoop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author informatics
 */
public class TestWriteFrient {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend f1 = new Friend("pennapa", 10, "99999999");
            Friend f2 =new Friend("Ricado", 40, "099999999");
            System.out.println(f1);
            System.out.println(f2);
            File file = new File("Frients.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(f1);
            oos.writeObject(f2);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFrient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteFrient.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFrient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
