package com.werapan.oxoop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author informatics
 */
public class TestWritePlayer{
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Player o = new Player('O');
        Player x = new Player('X');
        o.getWin();
        o.getLoss();
        o.getDraw();
        x.getWin();
        x.getLoss();
        x.getDraw();
        System.out.println(o);
        System.out.println(x);
        
        File f = new File("Player.dat");
        FileOutputStream fos = new FileOutputStream(f);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(o);
        oos.writeObject(x);
        oos.close();
        fos.close();
        
        
    }
}
